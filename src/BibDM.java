import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.*;
public class BibDM{

	/**
	 * Ajoute deux entiers
	 * @param a le premier entier à ajouter
	 * @param b le deuxieme entier à ajouter
	 * @return la somme des deux entiers
	 */
	public static Integer plus(Integer a, Integer b){
		return a+b;
	}


	/**
	 * Renvoie la valeur du plus petit élément d'une liste d'entiers
	 * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
	 * @param liste
	 * @return le plus petit élément de liste
	 */
	public static Integer min(List<Integer> liste){
		if (liste.size()>0){
			Integer min = liste.get(0);
			for (Integer elem : liste){
				if(min > elem ){
					min = elem;
				}
			}
			return min;
		}
		else{
			return null;
		}
	}
	
	
	/**
	 * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
	 * @param valeur 
	 * @param liste
	 * @return true si tous les elements de liste sont plus grands que valeur.
	 */
	public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
		if (liste.size()>0){
			for (T elem : liste){
				if (elem.compareTo(valeur)<0 || elem.compareTo(valeur)==0) {
					return false;
				}
			}
			return true;
		}
		else{
			return true; 
		}
	}



	/**
	 * Intersection de deux listes données par ordre croissant.
	 * @param liste1 une liste triée
	 * @param liste2 une liste triée
	 * @return une liste triée avec les éléments communs à liste1 et liste2
	 */
	public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
		List<T> res = new ArrayList<>();
		int cont = 0;
		int i = 0;
		int j = 0;
		while(i<liste1.size()){
			if (liste1.get(i).equals(liste2.get(j)) && !res.contains(liste1.get(i))){
				res.add(liste1.get(i));
				i++;
				j++;
			}
			else{
				cont = liste1.get(i).compareTo(liste2.get(j));
				if (cont > 0){
					j++;
				}
				else{
					i++;
				}
			}
		}
		return res;
	}


	/**
	 * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
	 * @param texte une chaine de caractères
	 * @return une liste de mots, correspondant aux mots de texte.
	 */
	public static List<String> decoupe(String texte){
		List<String> res = new ArrayList<String>(); 
		String[] tab = texte.split(" ");
		for ( int i=0 ; i<tab.length ; i++){
			if (tab[i].length() >=2){
				res.add(tab[i]);
			}
		}
		return res;
	}



	/**
	 * Renvoie le mot le plus présent dans un texte.
	 * @param texte une chaine de caractères
	 * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
	 */

	public static String motMajoritaire(String texte){
		if (texte==""){
			return null;
		}
		List<String> TexteDecoupe = new ArrayList<>();
		TexteDecoupe = decoupe(texte);
		Map<String,Integer> dico = new HashMap<>();
		for (String mot : TexteDecoupe){
			if ( dico.containsKey(mot)){
				dico.replace(mot, dico.get(mot)+1);
			}
			else{
				dico.put(mot, 1);
			}
		}
		TreeMap trieur = new TreeMap(dico);
		trieur.descendingKeySet();
		return (String) trieur.firstKey();
	}
	
	/**
	 * Permet de tester si une chaine est bien parenthesée
	 * @param chaine une chaine de caractères composée de ( et de )
	 * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
	 */
	public static boolean bienParenthesee(String chaine){
		int Para = 0;
		if (chaine == "")
			return true;
		else if (!(String.valueOf(chaine.charAt(0)).equals("(") && String.valueOf(chaine.charAt(chaine.length()-1)).equals(")")))
			return false;
		else{ 
			for (int i = 0; i<chaine.length(); i++){
				if (String.valueOf(chaine.charAt(i)).equals("("))
					Para+=1;
				else
					Para-=1;
			}
		}
		return Para == 0;
	}
	
	/**
	 * Permet de tester si une chaine est bien parenthesée
	 * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
	 * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
	 */
	public static boolean bienParentheseeCrochets(String chaine){
		int Para = 0;
		int Cro = 0;
		if ( chaine ==""){
			return true;
		}
		else{
			if ((String.valueOf(chaine.charAt(0)).equals(")") || String.valueOf(chaine.charAt(0)).equals("]")) || (String.valueOf(chaine.charAt(chaine.length()-1)).equals("(") || String.valueOf(chaine.charAt(chaine.length()-1)).equals("["))){
				return false;
			}
			else{
				if (String.valueOf(chaine.charAt(0)).equals("(")){
					Para+=1;
				}
				else{
					Cro+=1;
				}
				for (int i=1 ; i<chaine.length() ; i++){
					if ((String.valueOf(chaine.charAt(i-1)).equals("(") && String.valueOf(chaine.charAt(i)).equals("]")) || (String.valueOf(chaine.charAt(i-1)).equals("[") && String.valueOf(chaine.charAt(i)).equals(")"))){
						return false;
					}
					else{
						if (String.valueOf(chaine.charAt(i)).equals("(")){
							Para+=1;
						}
						else{
							if (String.valueOf(chaine.charAt(i)).equals(")")){
								Para-=1;
							}
							else{
								if (String.valueOf(chaine.charAt(i)).equals("[")){
									Cro+=1;
								}
								else{
									Cro-=1;
								}
							}
						}
					}
				}
			}
		}
		return Para == 0 && Cro == 0;
	}

	


	/**
	 * Recherche par dichtomie d'un élément dans une liste triée
	 * @param liste, une liste triée d'entiers
	 * @param valeur un entier
	 * @return true si l'entier appartient à la liste.
	 */
	public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
			int fin = 0;
			int debut = liste.size()-1;
			if (liste.size() > 1){
				while (fin < debut ){
					int milieu = (fin+debut)/2;
					if ( liste.get(milieu) < valeur ){
						fin = milieu +1;
					}
					else{
						debut = milieu;
					}
				}
				return fin < liste.size() && valeur == liste.get(fin);
			}
			else{
				return false;
			}
	}
}
